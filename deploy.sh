#!/bin/bash

DIR_PATH="/home/ubuntu/var/app/"
DEPLOY_PATH="/home/ubuntu/var/app/deploy"

if [ -d $DEPLOY_PATH ]
then
        cd $DEPLOY_PATH
        git pull origin main
else
        cd $DIR_PATH
        git clone https://gitlab.com/internet-engineering-course/deploy.git
fi

cd $DEPLOY_PATH

sudo docker pull javidjavid/note-backend:latest
sudo docker pull javidjavid/note-frontend:latest

sudo docker-compose -f /home/ubuntu/var/app/deploy/docker-compose.yml up -d
